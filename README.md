# np

[![Build Status](https://ci.kernel.fun/api/badges/cmiksche/np/status.svg)](https://ci.kernel.fun/cmiksche/np)

Fast way for creating a new post with static site generators like Hugo

[![asciicast](np.webp)](https://asciinema.org/a/Uwe3vWTTuJDYxtN2hlDSEX0yE)

## Install

Windows:

    go install

Linux:

    GOBIN=/usr/local/bin/ go install

## Development

Install pre-commit hooks:

    pre-commit install

Run program:

    go run main.go