package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func getArgsAsString(args []string) string {
	argString := ""
	for _, arg := range args {
		empty := ""
		if argString != "" {
			empty = " "
		}
		argString = argString + empty + arg
	}
	return argString
}

func convertTitleToFileName(title string) string {
	title = strings.ToLower(title)
	title = strings.Replace(title, "ä", "ae", -1)
	title = strings.Replace(title, "ö", "oe", -1)
	title = strings.Replace(title, "ü", "ue", -1)
	title = strings.Replace(title, "ß", "ss", -1)
	title = strings.Replace(title, " ", "-", -1)
	title = strings.Replace(title, ",", "", -1)
	title = strings.Replace(title, ":", "", -1)
	title = strings.Replace(title, ";", "", -1)
	title = title + ".md"
	return title
}

func convertArgsToTags(args []string) string {
	result := ""
	for _, arg := range args {
		empty := ""
		if result != "" {
			empty = ","
		}
		result = result + empty + "'" + arg + "'"
	}
	return "[" + result + "]"
}

func createNewPostFile(fileTitle string, originalTitle string, tags string) {
	dt := time.Now()
	f, err := os.Create(filepath.Join("content", "post", fileTitle))
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	d := []string{"---", "title: '" + originalTitle + "'",
		"date: \"" + dt.Format("2006-01-02") + "\"",
		"draft: true", "tags: " + tags, "---"}

	for _, v := range d {
		fmt.Fprintln(f, v)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}

func main() {
	args := os.Args[1:]
	originalTitle := getArgsAsString(args)
	fileTitle := convertTitleToFileName(originalTitle)
	tags := convertArgsToTags(args)
	createNewPostFile(fileTitle, originalTitle, tags)
}
